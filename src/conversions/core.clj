(ns conversions.core
  (:gen-class))

;; Conversion by unit, pick your conversion from a list, enter a value and get a result
(def CONVERSION_TABLE_HEADER_INDEX "Index")
(def CONVERSION_TABLE_HEADER_DESCRIPTION "Description")
(def INPUT_PROMPT_TEXT "choose>")
(def CONVERSION_PROMPT_TEXT "Enter value to convert:")

(defn round-tricky-floats
  [value]
  (/ (Math/round (* 100 value)) 100.0))

(defn convert-celsius-to-fahrenheit
  [value]
  (round-tricky-floats
   (+ (* 1.8 value) 32)))

(defn convert-dozen-to-actual
  [value]
  (round-tricky-floats
   (* 12.0 value)))

(def conversion-table
  [{:label "Celsius to Fahrenheit"
    :function convert-celsius-to-fahrenheit
    :input :float}
   {:label "Dozen to actual"
    :function convert-dozen-to-actual
    :input :integer}])

(defn print-conversions
  []
  (println (str CONVERSION_TABLE_HEADER_INDEX " " CONVERSION_TABLE_HEADER_DESCRIPTION))
  (dorun
   (map #(println (format "%d:    %s" (first %) (:label (last %))))
        (zipmap (range (count conversion-table)) conversion-table))))

(defmacro read-user-input
  [type]
  `(let [user-input# (read-line)
        trimmed-user-input# (clojure.string/trim user-input#)]
     (new ~type trimmed-user-input#)))

(defn read-user-input-int
  "Return the user input as an integer, throws if conversion fails."
  []
  (read-user-input Integer))

(defn read-user-input-float
  "Return the user input as a float, throws if conversion fails"
  []
  (read-user-input Float))

(defn print-input-prompt
  [text]
  (print (str text " "))
  (flush))

(defn attempt-conversion
  [index]
  (if-let [conversion (get conversion-table index)]
    (do 
      (print-input-prompt CONVERSION_PROMPT_TEXT)
      (let [user-input (read-user-input-float)]
        (try
          (println ((:function conversion) user-input))
          (catch Exception e (println (str "Conversion error: " (.getMessage e)))))))
    (println "Unknown conversion index")))

(defn -main
  [& args]
  (print-conversions)
  (while true
    (try
      (print-input-prompt INPUT_PROMPT_TEXT)
      (let [user-input (read-user-input-int)]
        ;; If we are here then the input is a number
        (attempt-conversion user-input))
      (catch Exception e (println  (str "Input error: " (.getMessage e)))))))
