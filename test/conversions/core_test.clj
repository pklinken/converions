(ns conversions.core-test
  (:require [clojure.test :refer :all]
            [conversions.core :refer :all]))

(defn create-reader-with-string
  [string]
  (let [reader (java.io.StringReader. string)]
    (java.io.BufferedReader. reader)))

(defmacro test-with-stdin-override
  [text body]
  `(with-open [pushback-reader# (create-reader-with-string ~text)]
    (binding [*in* pushback-reader#]
      ~body)))

(deftest user-input
  (testing "read-user-input-int"
    (test-with-stdin-override
     "1\n"
     (is (= 1 (read-user-input-int))))
    (test-with-stdin-override
     " 3\n"
     (is (= 3 (read-user-input-int))))
    (test-with-stdin-override
     "fzzbxṭ\n"
     (is (thrown? java.lang.NumberFormatException (read-user-input-int)))))
  
  (testing "read-user-input-float"
    (test-with-stdin-override
     "1.5\n"
     (is (= 1.5 (read-user-input-float))))
    (test-with-stdin-override
     "0\n"
     (is (= 0.0 (read-user-input-float))))
    (test-with-stdin-override
     "vzzzzbx\n"
     (is (thrown? java.lang.NumberFormatException (read-user-input-float))))))

(deftest print-conversion
  (testing "Print conversion table"
    (with-open [output-stream (java.io.ByteArrayOutputStream.)
                writer (java.io.OutputStreamWriter. output-stream)]
      (binding [*out* writer]
        (print-conversions)
        (let [output (.toString output-stream)
              output-lines (clojure.string/split output #"\n")
              header (first output-lines)
              body (clojure.string/join (drop 1 output-lines))]
          (let [re (java.util.regex.Pattern/compile (str CONVERSION_TABLE_HEADER_INDEX " " CONVERSION_TABLE_HEADER_DESCRIPTION))]
            (is (re-matches re header)))
          (let [re #"(\d+:\s.*)+"]
            (is (re-matches re body))))))))

(deftest input-prompt
  (testing "Input prompt"
    (with-open [output-stream (java.io.ByteArrayOutputStream.)
                writer (java.io.OutputStreamWriter. output-stream)]
      (binding [*out* writer]
        (print-input-prompt "test text")
       (let [output (.toString output-stream)]
         (let [re (java.util.regex.Pattern/compile (str "test text" "\\s+"))]
           (is (re-matches re output))))))))

(deftest conversion-functions
  (testing "round-tricky-floats"
    (is (= 25.0 (round-tricky-floats 25.0000005)))
    (is (= 0.0 (round-tricky-floats -0.00000001))))

  (testing "convert-fahrenheit-to-celsius"
    (let [celsius 5]
      (is (= 41.0 (convert-celsius-to-fahrenheit celsius))))
    (let [celsius 26]
      (is (= 78.8 (convert-celsius-to-fahrenheit celsius)))))

  (testing "convert-dozen-to-actual"
    (let [dozen 0.5]
      (is (= 6.0 (convert-dozen-to-actual dozen))))
    (let [dozen 1]
      (is (= 12.0 (convert-dozen-to-actual dozen))))))

